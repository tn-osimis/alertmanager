#!/usr/bin/env bash
exec alertmanager --config.file=<(echo "$CONFIG") "$@"
